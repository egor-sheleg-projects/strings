﻿using System;
using System.Globalization;

namespace Strings
{
    public static class Manipulations
    {
        /// <summary>
        /// Returns a greeting.
        /// </summary>
        public static string GetHelloGreeting1(string name)
        {
            const string template = "Hello, !";
            return template.Insert(7, name);
        }

        /// <summary>
        /// Returns a greeting.
        /// </summary>
        public static string GetGreeting1(string greeting, string name)
        {
            if (greeting is null)
            {
                throw new ArgumentNullException(nameof(greeting));
            }
            else if (name is null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            const string template = ", !";
            string mod1 = template.Insert(0, greeting);
            return mod1.Insert(greeting.Length + 2, name.ToUpperInvariant());
        }

        /// <summary>
        /// Returns a greeting.
        /// </summary>
        public static string GetHelloGreeting2(string name)
        {
            if (name is null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            const string template = "Hello, !";
            return template.Insert(7, name.Trim());
        }

        /// <summary>
        /// Returns a greeting.
        /// </summary>
        public static string GetGreeting2(string greeting, string name)
        {
            if (greeting is null)
            {
                throw new ArgumentNullException(nameof(greeting));
            }
            else if (name is null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            const string template = ", !";
            string mod1 = greeting.Trim();
            string mod2 = template.Insert(0, mod1);
            string mod3 = name.Trim();
            return mod2.Insert(mod1.Length + 2, mod3.ToLowerInvariant());
        }

        /// <summary>
        /// Returns a greeting.
        /// </summary>
        public static string GetHelloGreeting3(string template, string name)
        {
            if (template is null)
            {
                throw new ArgumentNullException(nameof(template));
            }

            return template.Replace("{name}", name, StringComparison.InvariantCulture);
        }

        /// <summary>
        /// Returns a greeting.
        /// </summary>
        public static string GetGreeting3(string template, string greeting, string name)
        {
            if (template is null)
            {
                throw new ArgumentNullException(nameof(template));
            }
            else if (name is null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            string mod1 = template.Replace("{greeting}", greeting.ToLowerInvariant(), StringComparison.InvariantCulture);
            return mod1.Replace("{name}", name.ToUpperInvariant(), StringComparison.InvariantCulture);
        }

        /// <summary>
        /// Returns refined code - without zero characters.
        /// </summary>
        public static string GetRefinedCode(string code)
        {
            if (code is null)
            {
                throw new ArgumentNullException(nameof(code));
            }

            return code.Remove(3, 3);
        }

        /// <summary>
        /// Returns refined date - without dash characters.
        /// </summary>
        public static string GetRefinedDate(string date)
        {
            if (date is null)
            {
                throw new ArgumentNullException(nameof(date));
            }

            string mod1 = date.Remove(2, 1);
            return mod1.Remove(4, 1);
        }
    }
}
