﻿using System;

namespace Strings
{
    public static class CopyingStrings
    {
        /// <summary>
        /// Copies one character from the <paramref name="source"/> to the <paramref name="destination"/>.
        /// </summary>
        public static string CopyOneChar(string source, string destination)
        {
            if (destination is null)
            {
                throw new ArgumentNullException(nameof(destination));
            }
            else if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            char[] destinationArray = destination.ToCharArray();
            source.CopyTo(0, destinationArray, 4, source.Length);
            return new string(destinationArray);
        }

        /// <summary>
        /// Copies three characters from the <paramref name="source"/> to the <paramref name="destination"/>.
        /// </summary>
        public static string CopyThreeChars(string source, string destination)
        {
            if (destination is null)
            {
                throw new ArgumentNullException(nameof(destination));
            }
            else if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            char[] destinationArray = destination.ToCharArray();
            source.CopyTo(0, destinationArray, 0, source.Length);
            return new string(destinationArray);
        }

        /// <summary>
        /// Copies five characters from the <paramref name="source"/> to the <paramref name="destination"/>.
        /// </summary>
        public static string CopyFiveChars(string source, string destination)
        {
            if (destination is null)
            {
                throw new ArgumentNullException(nameof(destination));
            }
            else if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            char[] destinationArray = destination.ToCharArray();
            source.CopyTo(0, destinationArray, 4, 5);
            return new string(destinationArray);
        }

        /// <summary>
        /// Copies six characters from the <paramref name="source"/> to the <paramref name="destination"/>.
        /// </summary>
        public static string CopySixChars(string source, string destination)
        {
            if (destination is null)
            {
                throw new ArgumentNullException(nameof(destination));
            }
            else if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            char[] destinationArray = destination.ToCharArray();
            source.CopyTo(2, destinationArray, 5, source.Length - 4);
            return new string(destinationArray);
        }

        /// <summary>
        /// Gets a production code by copying substrings of the <paramref name="regionCode"/>, <paramref name="locationCode"/>, <paramref name="dateCode"/> and <paramref name="factoryCode"/> parameters to the <paramref name="template"/>.
        /// </summary>
        public static string GetProductionCode(string template, string regionCode, string locationCode, string dateCode, string factoryCode)
        {
            if (template is null)
            {
                throw new ArgumentNullException(nameof(template));
            }
            else if (regionCode is null)
            {
                throw new ArgumentNullException(nameof(regionCode));
            }
            else if (locationCode is null)
            {
                throw new ArgumentNullException(nameof(locationCode));
            }
            else if (dateCode is null)
            {
                throw new ArgumentNullException(nameof(dateCode));
            }
            else if (factoryCode is null)
            {
                throw new ArgumentNullException(nameof(factoryCode));
            }

            char[] destinationArray = template.ToCharArray();
            regionCode.CopyTo(1, destinationArray, 0, 1);
            locationCode.CopyTo(4, destinationArray, 3, 2);
            dateCode.CopyTo(3, destinationArray, 7, 3);
            factoryCode.CopyTo(2, destinationArray, 12, 4);

            return new string(destinationArray);
        }
    }
}
